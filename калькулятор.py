import tkinter as tk
window=tk.Tk()
window.title('Калькулятор')


textEntry = tk.StringVar()
Ent1 = tk.Entry(width=20,  textvariable = textEntry)
Ent1.grid(row=0, column=0, columnspan=3)

def plus1():
    global Ent1, textEntry
    try:
        number = int(Ent1.get())
        textEntry.set(number+1)
    except ValueError:
        print("Ошибка! Введите цифры")

def plus2():
    global Ent1, textEntry
    try:
        number = int(Ent1.get())
        textEntry.set(number+2)
    except ValueError:
        print("Ошибка! Введите цифры")

def plus3():
    global Ent1, textEntry
    try:
        number = int(Ent1.get())
        textEntry.set(number+3)
    except ValueError:
        print("Ошибка! Введите цифры")

def minus1():
    global Ent1, textEntry
    try:
        number = int(Ent1.get())
        textEntry.set(number-1)
    except ValueError:
        print("Ошибка! Введите цифры")

def minus2():
    global Ent1, textEntry
    try:
        number = int(Ent1.get())
        textEntry.set(number-2)
    except ValueError:
        print("Ошибка! Введите цифры")

def minus3():
    global Ent1, textEntry
    try:
        number = int(Ent1.get())
        textEntry.set(number-3)
    except ValueError:
        print("Ошибка! Введите цифры")

def increase2():
    global Ent1, textEntry
    try:
        number = int(Ent1.get())
        textEntry.set(number*2)
    except ValueError:
        print("Ошибка! Введите цифры")

def increase3():
    global Ent1, textEntry
    try:
        number = int(Ent1.get())
        textEntry.set(number*3)
    except ValueError:
        print("Ошибка! Введите цифры")

def Del():
    Ent1.delete(0, 'end')
 
But1=tk.Button(window,text='+1', font=("Arial",10),command=lambda:plus1()).grid(row=1,column=0)

But2=tk.Button(window,text='+2', font=("Arial",10),command=lambda:plus2()).grid(row=1, column=1)

But3=tk.Button(window,text='+3', font=("Arial",10),command=lambda:plus3()).grid(row=1, column=2)

But4=tk.Button(window,text='- 1', font=("Arial",10),command=lambda:minus1()).grid(row=2, column=0)

But5=tk.Button(window,text='- 2', font=("Arial",10),command=lambda:minus2()).grid(row=2, column=1)

But6=tk.Button(window,text='- 3', font=("Arial",10),command=lambda:minus3()).grid(row=2, column=2)

But7=tk.Button(window,text='* 2', font=("Arial",10),command=lambda:increase2()).grid(row=3, column=0)

But8=tk.Button(window,text='* 3', font=("Arial",10),command=lambda:increase3()).grid(row=3, column=1)

But9=tk.Button(window,text='= 0', font=("Arial",10),command=Del).grid(row=3, column=2)

window.mainloop()
