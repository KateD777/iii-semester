﻿//Операции в БД: поиск удаление добавление

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <stack>

struct Tree
{
    int data;
    Tree* left;
    Tree* right;
    Tree* parent;
};

// Инициализация вершины
Tree* MAKE(int data, Tree* p)
{
    Tree* q = new Tree;         // Выделяем память под вершину
    q->data = data;
    q->left = nullptr;
    q->right = nullptr;
    q->parent = p;
    return q;
}

// Добавление элемента
Tree* ADD(int data, Tree*& root)
{
    if (root == nullptr)
    {
        root = MAKE(data, nullptr);
        return root;
    }
    Tree* v = root;
    while ((data < v->data && v->left != nullptr) || (data > v->data && v->right != nullptr))
    {
        if (data < v->data)
        {
            v = v->left;
        }
        else
        {
            v = v->right;
        }
    }

    if (data == v->data)
        return nullptr;

    Tree* u = MAKE(data, v);
    if (data < v->data)
    {
        v->left = u;
    }
    else
    {
        v->right = u;
    }
    return u;

}

//Обходы дерева
void PASS(Tree* v)
{
    if (v == nullptr)
        return;
    std::cout << v->data << " ";
    PASS(v->left);
    PASS(v->right);
}

void NON_RECURS(Tree*& v)
{
    std::stack<Tree*> number;
    number.push(v);
    while (number.empty() == false)
    {
        Tree* t = number.top();
        number.pop();
        std::cout << t->data << " ";
        if (t->right)
            number.push(t->right);
        if (t->left)
            number.push(t->left);
    }
}

void CENTER(Tree*& v) 
{
    if (v->left != nullptr) 
        CENTER(v->left);

    std::cout << v->data << " ";

    if (v->right != nullptr) 
        CENTER(v->right);
}

void REVERS(Tree*& v) 
{
    if (v->left != nullptr) 
        REVERS(v->left);

    if (v->right != nullptr)
        REVERS(v->right);

    std::cout << v->data << " ";
}

// Поиск
Tree* SEARCH(int data, Tree* v) // v - элемент, от которого начинаем поиск
{
    if (v == nullptr)
        return v;
    if (v->data == data)
        return v;
    if (data < v->data)
        return SEARCH(data, v->left);
    else
        return SEARCH(data, v->right);
}

// Удаление
void DELETE(int data, Tree*& root)  
{
    Tree* v = SEARCH(data, root);
    if (v == nullptr)
        return;

    if (v->left == nullptr && v->right == nullptr)
    {
        delete root;
        root = nullptr;
        return;
    }
    else if (v->left == nullptr && v->right != nullptr)
    {
        Tree* t = v->right;
        while (t->left != nullptr)
            t = t->left;
        v->data = t->data;
        v = t;
    }

    else if (v->left != nullptr && v->right == nullptr)
    {
        Tree* t = v->left;
        while (t->right != nullptr)
            t = t->right;
        v->data = t->data;
        v = t;
    }

    else if (v->left != nullptr && v->right != nullptr)
    {
        Tree* t = v->right;
        while (t->left != nullptr)
            t = t->left;
        v->data = t->data;
        v = t;
    }

    Tree* t;
    if (v->left == nullptr)
        t = v->right;
    else
        t = v->left;
    if (v->parent->left == v)
        v->parent->left = t;
    else
        v->parent->right = t;
    if (t != nullptr)
        t->parent = v->parent;
    delete v;

}

// Очистка
void CLEAR(Tree*& v)
{
    if (v == nullptr)
        return;

    CLEAR(v->left);

    CLEAR(v->right);

    delete v;
    v = nullptr;
}

int main()
{
    setlocale(LC_ALL, "RUS");
    std::ifstream file_in("input.txt");
    std::fstream file_out("output.txt");
    std::string line;
    int level = 0;
    int mas[100];
    Tree* root = nullptr;

    while (getline(file_in, line))
    {

        if (line[0] == '+')
        {
            line.erase(0, 1);
            int end = atoi(line.c_str());
            ADD(end, root);
        }

        else if (line[0] == '-')
        {
            line.erase(0, 1);
            int end = atoi(line.c_str());
            DELETE(end, root);
        }

        else if (line[0] == '?')
        {
            line.erase(0, 1);
            int end = atoi(line.c_str());
            Tree* p = SEARCH(end, root);

            if (p != nullptr)
            {
                level++;
                file_out << "-" << level << "(" << end << ")";
            }
            else
                file_out << 'n';
        }

        else if (line[0] == 'E')
        {
            break;
        }
    }

    std::cout << "Прямой: ";
    PASS(root);
    std::cout << "\nНе рекурсивный: ";
    NON_RECURS(root);

    std::cout << "\nЦентральный: ";
    CENTER(root);

    std::cout << "\nОбратный: ";
    REVERS(root);


    if (file_out)
    {
        std::ifstream file_out("output.txt");
        while (!file_out.eof())
        {
            std::string out;
            file_out >> out;
            std::cout << out;
        }
    }
    else
        std::cout << "Файл пуст";

    file_in.close();
    file_out.close();


    CLEAR(root);
    PASS(root);
    std::cout << "\nОчистка прошла успешно!";

    return 0;
}