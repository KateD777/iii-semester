﻿#include <iostream>
#include <fstream>
#include <stack>
#include <string>

//на вход подается строка, состоящая их скобок. Программа должна определить правильность введеного скобочного выражения. 
//Программа должна работать на русском языке:Введите строку, Строка не сущ-ет, Строка сущ-ет и тп.
//пункт 1: в строке будут скобки только одного типа: или (), или [], или {}
//пункт 2: в строке будут все виды скобок

int main()
{
    setlocale(LC_ALL, "RUS");
    std::ifstream data("data.txt"); // открыли файл для чтения
    std::string str;
    while (getline(data, str))
    {
        std::cout << str;
        int k_0 = 0;
        int k_1 = 0;
        int k_2 = 0;
        int Hapyw = 0;
        bool flage0 = false;
        bool flage1 = false;
        bool flage2 = false;
        for (int i = 0; i < str.length(); ++i) 
        {
            if (str[i] == '(')
            {
                ++k_0;
                flage0=true;
            }
            else if (str[i] == '{')
            {
                ++k_1;
                flage1 = true;
            }
            else if (str[i] == '[')
            {
                ++k_2;
                flage2 = true;
            }
          
                else if (str[i] == ')')
                    --k_0;
                else if (str[i] == '}')
                    --k_1;
                else if (str[i] == ']')
                    --k_2;
                else std::cout << "\nВстретился посторонний элемент: " << Hapyw+1 << std::endl;
        }
        if (k_0 == 0 && flage0 == true)
            std::cout << "\nПарное кол-во ) скобок существует" << std::endl;
        if (k_1 == 0 && flage1 == true)
            std::cout << "\nПарное кол-во { скобок существует" << std::endl;
        if (k_2 == 0 && flage2 == true)
            std::cout << "\nПарное кол-во ] скобок существует" << std::endl;
        else
            std::cout << "\nСтрока из парных скобок не существует" << std::endl;

    }
}