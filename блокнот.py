from tkinter import *
from tkinter import messagebox
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename
root = Tk()
root.title('Блокнотик :)')
def open_fuc():
 global text
 fname=askopenfilename(title='Выбор файла', filetypes=[('text files', '*.txt'), ("all 
files", "*")])
 with open (fname) as f:
 text.delete('1.0', 'end')
 text.insert(1.0, f.read())
def on_closing():
 global root, text
 fname=asksaveasfilename(title='Название и куда поместить', filetypes=[('text files', 
'*.txt'), ("all files", "*")])
 f = open(fname, 'w')
 text = text.get('1.0', 'end')
 f.write(text)
 f.close()
 if messagebox.askokcancel("Выход", "Вы точно хотите выйти?"):
 root.destroy()
def save_fuc():
 global text
 fname=asksaveasfilename(title='Название и куда поместить', filetypes=[('text files', 
'*.txt'), ("all files", "*")])
 f = open(fname, 'w')
 text = text.get('1.0', 'end')
 f.write(text)
 f.close()
mainmenu = Menu(root) 
root.config(menu=mainmenu) 
filemenu = Menu(mainmenu, tearoff=0)
filemenu.add_command(label="Открыть...", command=open_fuc)
filemenu.add_command(label="Сохранить...", command=save_fuc)
filemenu.add_separator()
filemenu.add_command(label="Выход", command=on_closing)
helpmenu = Menu(mainmenu, tearoff=0)
helpmenu.add_command(label="Исполнитель Деревенко Екатерина")
mainmenu.add_cascade(label="Файл",
 menu=filemenu)
mainmenu.add_cascade(label="Справка",
 menu=helpmenu)
text = Text(root)
text.grid(row=0, column=0)
text.pack(side=LEFT)
scroll = Scrollbar(command=text.yview)
scroll.pack(side=LEFT, fill=Y)
text.config(yscrollcommand=scroll.set)
root.protocol("WM_DELETE_WINDOW", on_closing)
root.mainloop()
