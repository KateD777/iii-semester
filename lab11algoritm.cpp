﻿//Разделить массив на две части следующим образом: все элементы из левой части, которые больше или равны опорному, 
//перекидываем в правую, аналогично, все элементы из правой, которые меньше или равны опорному кидаем в левую часть.
#include <iostream>
#include <cstdlib>

bool QuickSort(int mas[], int nStart, int nEnd)         //Сложность быстрой сортировки   
{														//Худший O(n^2)
	int L, R, c, X;										//Средний O(n)
	if (nStart >= nEnd)									//Лучший O(n log n)
		return 0;

	L = nStart;
	R = nEnd;
	X = mas[(L + R) / 2];
	while (L <= R)
	{
		while (mas[L] < X)
			L++;
		while (mas[R] > X)
			R--;
		if (L <= R)
		{
			c = mas[L];
			mas[L] = mas[R];
			mas[R] = c;
			L++;
			R--;
		}
	}
	QuickSort(mas, nStart, R);
	QuickSort(mas, L, nEnd);
}

int main()
{
	setlocale(LC_ALL, "Rus");
	int N = 100;
	int* mas = new int[N];
	std::cout << "Ваш массив: ";
	for (int o = 0; o < N; o++)
	{
		std::cout << " [" << o + 1 << "]" << " ";
		mas[o] = rand() % N + 1;
		std::cout << mas[o];
	}
	QuickSort(mas, 0, N - 1);
	std::cout << '\n' << "Полученный массив: ";
	for (int o = 0; o < N; ++o)
		std::cout << " [" << o + 1 << "]" << mas[o] << " ";
	delete[] mas;
	return 0;
}

/*std::cout << "Количество элементов в массиве > ";         если введить с клавиатуры
	std::cin >> n;
	int* mas = new int[n];
	for (i = 0; i < n; i++) //ввод массива
	{
		std::cout << i + 1 << " элемент > ";
		std::cin >> mas[i];
	}*/