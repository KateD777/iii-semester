﻿#include "iostream"
//сортировка поразрядная
//Массив несколько раз перебирается и элементы перегруппировываются в зависимости от того,
//какая цифра находится в определённом разряде. После обработки разрядов (всех или почти всех) массив оказывается упорядоченным.
const int n = 100, col_razr = 3;
int velich_razr(int chislo, int razr)   
{
    while (razr > 1)
    {
        chislo /= 10;
        razr--;
    }
    return chislo % 10;
}

void sort_razr(int dop_mas[n][n], int mas[n], int razr)
{
    int mas_col[n], i, j, temp = 0;

    for (i = 0; i < n; i++)
        mas_col[i] = 0;

    for (i = 0; i < n; i++)
    {
        int a = velich_razr(mas[i], razr);
        dop_mas[ mas_col[a] ][a] = mas[i];
        mas_col[a]++;
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < mas_col[i]; j++)
        {
            mas[temp] = dop_mas[j][i];
            temp++;
        }
    }
}

int main()
{
    setlocale(LC_ALL, "Rus");
    int razr, i;
    int N = 100;
    int* mas = new int[N];
    int dop_mas[n][n];
    std::cout << "Ваш массив: ";
    for (int o = 0; o < N; o++)
    {
        std::cout << " [" << o + 1 << "]" << " ";
        mas[o] = rand() % N + 100;
        std::cout << mas[o];
    }
    for (razr = 1; razr < 4; razr++)
        sort_razr(dop_mas, mas, razr);
    std::cout << '\n' << "Полученный массив: ";
    for (int o = 0; o < N; ++o)
        std::cout << " [" << o + 1 << "]" << mas[o] << " ";
    delete[] mas;
}

/*std::cout << "Количество элементов в массиве > ";         если введить с клавиатуры
    std::cin >> n;
    int* mas = new int[n];
    for (i = 0; i < n; i++) //ввод массива
    {
        std::cout << i + 1 << " элемент > ";
        std::cin >> mas[i];
    }*/
