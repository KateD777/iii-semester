﻿//сортировка методом причесывания
//это улучшенная сортирока пузырьком, равниваются элементы, которые стоят друг от друга на каком-то расстоянии. 
// Оно не слишком большое, чтобы сильно не откидывать элементы и возвращать потом большинство назад, 
// но и не слишком маленькое, чтобы можно было отправлять не на соседнее место, а чуть дальше.
//сначала сортировка с большим шагом, потом с шагом поменьше
#include <iostream>
#include <vector>

void comb(std::vector<int>& data) // data — название вектора  (передаём по ссылке, чтобы вызов comb(array) менял вектор array)
{
	double factor = 1.2473309; // фактор уменьшения
	int step = data.size() - 1; // шаг сортировки

	//Последняя итерация цикла, когда step==1 эквивалентна одному проходу сортировки пузырьком
	while (step >= 1)
	{
		for (int i = 0; i + step < data.size(); i++)
		{
			if (data[i] > data[i + step])
			{
				std::swap(data[i], data[i + step]);
			}
		}
		step /= factor;
	}
}

int main()
{
	srand(time(NULL));
	setlocale(LC_ALL, "RUS");
	const int N = 20;
	int i;
	std::vector<int>count(N);
	std::cout << "Вектор заполненный рандомно: ";
	for (i = 0; i < N; i++) 
	{
		count[i] = rand() % 100;
		std::cout << count[i] << " ";
	}

	comb(count);
	std::cout << '\n' << "Полученный отсортированный вектор: ";
	for (int o = 0; o < N; ++o)
		std::cout << " [" << o + 1 << "]" << count[o] << " ";
	return 0;
}

/*std::cout << "Количество элементов в массиве > ";         если вводить с клавиатуры
	std::cin >> n;
	int* mas = new int[n];
	for (i = 0; i < n; i++) //ввод массива
	{
		std::cout << i + 1 << " элемент > "; 
		std::cin >> mas[i];
	}*/