﻿#include <iostream>
#include <fstream>
#include <string>
#include <map>
//если хеш-функция выделяет один индекс сразу двум элементам, то храниться они будут в одном и том же индексе
int main()
{
    std::ifstream ifs("text.txt");
    std::map<std::string, int> table;
    std::string word;

    while (ifs >> word) 
    {
        table[word]++;
    }

    std::map<std::string, int>::iterator it;

    for (it = table.begin(); it != table.end(); ++it) 
    {
        std::cout << it->first << " " << it->second << std::endl;
    }

    return 0;
}