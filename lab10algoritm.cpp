﻿//сортировка слиянием
//Список для сортировки разбивается на два массива одинаковой длины, путем деления списка на элементы. 
//Если количество элементов в списке равно 0 или 1, то список считается отсортированным.
//Каждый подсписок сортируется индивидуально с помощью рекурсивной сортировки слиянием.
//Отсортированные подсписки затем объединяются или сливаются вместе, чтобы сформировать полный отсортированный список.
#include <iostream>

void merge(int*, int, int, int);

void merge_sort(int* arr, int low, int high)
{
    int mid;
    if (low < high) {
        //разделим массив посередине и отсортируем независимо, используя сортировку слиянием
        mid = (low + high) / 2;
        merge_sort(arr, low, mid);
        merge_sort(arr, mid + 1, high);
        //объединим отсортированные массивы
        merge(arr, low, high, mid);
    }
}
// Сортировка слиянием 
void merge(int* arr, int low, int high, int mid)
{
    int i, j, k, c[50];
    i = low;
    k = low;
    j = mid + 1;
    while (i <= mid && j <= high) {
        if (arr[i] < arr[j]) {
            c[k] = arr[i];
            k++;
            i++;
        }
        else {
            c[k] = arr[j];
            k++;
            j++;
        }
    }
    while (i <= mid) {
        c[k] = arr[i];
        k++;
        i++;
    }
    while (j <= high) {
        c[k] = arr[j];
        k++;
        j++;
    }
    for (i = low; i < k; i++) {
        arr[i] = c[i];
    }
}
// считывание входного массива и вызов mergesort
int main()
{
        setlocale(LC_ALL, "Rus");
        int N = 50;
        int* myarray = new int[N];
        std::cout << "Ваш массив: ";
        for (int o = 0; o < N; o++)
        {
            std::cout << " [" << o + 1 << "]" << " ";
            myarray[o] = rand() % N + 1;
            std::cout << myarray[o];
        }
        merge_sort(myarray, 0, N-1);
        std::cout << '\n' << "Полученный массив: ";
        for (int o = 0; o < N; ++o)
            std::cout << " [" << o + 1 << "]" << myarray[o] << " ";
        delete[] myarray;
}

/*std::cout << "Количество элементов в массиве > ";         если введить с клавиатуры
    std::cin >> n;
    int* mas = new int[n];
    for (i = 0; i < n; i++) //ввод массива
    {
        std::cout << i + 1 << " элемент > ";
        std::cin >> mas[i];
    }*/