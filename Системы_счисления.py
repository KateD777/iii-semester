import tkinter as tk
window=tk.Tk()
window.geometry('200x200')
window.title('Перевод в другую СС')

Ent1=tk.Entry(width=20)
Ent1.grid(row=0,column=1)
Ent1.pack()

def onclick():
    global Ent1
    a=int(Ent1.get())
    print(f'В 2-ой системе счисления {bin(a)}')
    print(f'В 8-ой системе счисления {oct(a)}')
    print(f'В 16-ой системе счисления {hex(a)}')

But1=tk.Button(window,text='Нажми меня', font=("Arial",10),command=onclick)
But1.pack()
window.mainloop()
