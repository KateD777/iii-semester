﻿//пирамидальная сортировка
//метод сортировки сравнением, основанный на такой структуре данных как двоичная куча. 
//Она похожа на сортировку выбором, где мы сначала ищем максимальный элемент и помещаем его в конец. 
//Далее мы повторяем ту же операцию для оставшихся элементов.

//Создать максимальную кучу из заданных данных таким образом, чтобы корень был самым высоким элементом кучи.
//Удалить корневой узел, то есть самый высокий элемент из кучи, и заменить его последним элементом кучи.
//Затем отрегулировать максимальную кучу, чтобы не нарушать свойства максимальной кучи(heapify).
//Приведенный выше шаг уменьшает размер кучи на 1.
//Повторить вышеуказанные три шага, пока размер кучи не уменьшится до 1.
#include <iostream>
using namespace std;

void heapify(int arr[], int n, int root)     
{
    int largest = root;

    int l = 2 * root + 1; // слева = 2*корень + 1
    if (l < n && arr[l] > arr[largest])          
        largest = l;

    int r = 2 * root + 2; // справа = 2*корень + 2
    if (r < n && arr[r] > arr[largest])             
        largest = r;

    // Если самый большой не является корневым
    if (largest != root)
    {
        swap(arr[root], arr[largest]);        

        heapify(arr, n, largest);
    }
}

// реализация сортировки кучи
void heapSort(int arr[], int n)
{
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    for (int i = n - 1; i >= 0; i--)
    {
        swap(arr[0], arr[i]);

      //для уменьшенной кучи
        heapify(arr, i, 0);
    }
}

int main()
{
    setlocale(LC_ALL, "Rus");
    int N = 100;
    int* heap_arr = new int[N];
    std::cout << "Ваш массив: ";
    for (int o = 0; o < N; o++)
    {
        std::cout << " [" << o + 1 << "]" << " ";
        heap_arr[o] = rand() % N + 1;
        std::cout << heap_arr[o];
    }

    heapSort(heap_arr, N);
    
    std::cout << '\n' << "Полученный массив: ";
    for (int o = 0; o < N; ++o)
        std::cout << " [" << o + 1 << "]" << heap_arr[o] << " ";
    delete[] heap_arr;
}

/*std::cout << "Количество элементов в массиве > ";         если введить с клавиатуры
    std::cin >> n;
    int* mas = new int[n];
    for (i = 0; i < n; i++) //ввод массива
    {
        std::cout << i + 1 << " элемент > ";
        std::cin >> mas[i];
    }*/