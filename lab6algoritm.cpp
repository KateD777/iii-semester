﻿//сортировка выбором
//состоит в поиске минимального значения элемента в массиве, и перемещения этого значения в начало массива.
//“Начало” в алгоритме с каждым шагом цикла смещается в сторону хвоста массива. Поэтому на первой итерации цикла, 
//найденное минимальное значение меняется местами со значением в нулевой ячейке массива.
//На второй итерации “начало” уже будет указывать на следующую(первую) ячейку и так далее.

#include <iostream>

void SelectionSort(int A[], int n) //сортировка выбором
{
	int count, key;
	for (int i = 0; i < n - 1; i++)
	{
		count = A[i]; 
		key = i;
		for (int j = i + 1; j < n; j++)
			if (A[j] < A[key])   //находим  мнимальное
				key = j;
		if (key != i)
		{
			A[i] = A[key];
			A[key] = count;
		}
	}
}
int main()
{
	setlocale(LC_ALL, "Rus");
	int N = 100;
	int* mas = new int[N];
	std::cout << "Ваш массив: ";
	for (int o = 0; o < N; o++)
	{
		std::cout << " [" << o + 1 << "]" << " ";
		mas[o] = rand() % N + 1;
		std::cout << mas[o];
	}
	SelectionSort(mas, N);
	std::cout << '\n' << "Полученный массив: ";
	for (int o = 0; o < N; ++o)
		std::cout << " [" << o + 1 << "]" << mas[o] << " ";
	delete[] mas;
	return 0;
}

/*std::cout << "Количество элементов в массиве > ";         если введить с клавиатуры
	std::cin >> n;
	int* mas = new int[n];
	for (i = 0; i < n; i++) //ввод массива
	{
		std::cout << i + 1 << " элемент > ";
		std::cin >> mas[i];
	}*/
